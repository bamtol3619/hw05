#include <set>
#include <iostream>
#include <stdlib.h>

using namespace std;

set<int> SetUnion(const set<int>& set0, const set<int>& set1){
	set<int> Union;
	for(set<int>::iterator it1=set0.begin();it1!=set0.end();++it1)	Union.insert(*it1);
	for(set<int>::iterator it2=set1.begin();it2!=set1.end();++it2)	Union.insert(*it2);
	return Union;
}

set<int> SetDifference(const set<int>& set0, const set<int>& set1){
	set<int> Difference;
	for(set<int>::iterator it1=set0.begin();it1!=set0.end();++it1)	Difference.insert(*it1);
	for(set<int>::iterator it2=set1.begin();it2!=set1.end();++it2)	Difference.erase(*it2);
	return Difference;
}

set<int> SetIntersection(const set<int>& set0, const set<int>& set1){
	set<int> Intersection;
	for(set<int>::iterator it1=set0.begin();it1!=set0.end();++it1){
		for(set<int>::iterator it2=set1.begin();it2!=set1.end();++it2){
			if(*it1==*it2)	Intersection.insert(*it1);
		}
	}
	return Intersection;
}

// return false when input is incorrect
// (input must be like { 1 2 3 })
bool InputSet(istream& in, set<int>* s){
	string inputs;
	in >> inputs;
	if(inputs=="{"){
		in >> inputs;
		while(inputs!="}"){
			(*s).insert(atoi(inputs.c_str()));
			in >> inputs;
		}
	return true;
	}
	else	return false;
}

void OutputSet(const set<int>& s, ostream& out){
	out << "{ ";
	for(set<int>::iterator it=s.begin();it!=s.end();++it)	out << *it << " ";
	out << "}" << endl;
}