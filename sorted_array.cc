#include <iostream>
#include <vector>
#include "sorted_array.h"

using namespace std;

SortedArray::SortedArray(){vector<int> numbers_;}
SortedArray::~SortedArray(){}

void SortedArray::AddNumber(int num){
	numbers_.push_back(num);	
}

vector<int> SortedArray::GetSortedAscending() const{
	vector<int> temp=numbers_;
	for(int i=0;i<temp.size()-1;++i){
		for(int j=i+1;j<temp.size();++j){
			if(temp[i]>temp[j]){
				int num=temp[i];
				temp[i]=temp[j];
				temp[j]=num;
			}
		}
	}
	return temp;
}

vector<int> SortedArray::GetSortedDescending() const{
	vector<int> temp=numbers_;
	for(int i=0;i<temp.size()-1;++i){
		for(int j=i+1;j<temp.size();++j){
			if(temp[i]<temp[j]){
				int num=temp[i];
				temp[i]=temp[j];
				temp[j]=num;
			}
		}
	}
	return temp;
}

int SortedArray::GetMax() const{
	int temp=numbers_[0];
	for(int i=0;i<GetSize();++i){
		if(temp<numbers_[i])	temp=numbers_[i];
	}
	return temp;
}

int SortedArray::GetMin() const{
	int temp=numbers_[0];
	for(int i=0;i<GetSize();++i){
		if(temp>numbers_[i])	temp=numbers_[i];
	}
	return temp;
}

int SortedArray::GetSize() const{return numbers_.size();}