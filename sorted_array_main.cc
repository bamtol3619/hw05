#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include "sorted_array.h"

using namespace std;

int main(){
	SortedArray Array;
	while(true){
		string input;
		cin >> input;
		if(input=="quit")	break;
		else if(input=="ascend"){
			for(int i=0;i<(Array.GetSortedAscending()).size();++i){
				cout << (Array.GetSortedAscending())[i] << " ";
			}
			cout << endl;
		}
		else if(input=="descend"){
			for(int i=0;i<(Array.GetSortedDescending()).size();++i){
				cout << (Array.GetSortedDescending())[i] << " ";
			}
			cout << endl;
		}
		else if(input=="max"){
			cout << Array.GetMax() << endl;
		}
		else if(input=="min"){
			cout << Array.GetMin() << endl;
		}
		else	Array.AddNumber(atoi(input.c_str()));
	}
	return 0;
}