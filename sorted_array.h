#include <iostream>
#include <vector>

using namespace std;

#ifndef __hw05__sorted_array__
#define __hw05__sorted_array__

class SortedArray {
	public:
		SortedArray();
		~SortedArray();

		void AddNumber(int num);

		vector<int> GetSortedAscending() const;
		vector<int> GetSortedDescending() const;
		int GetMax() const;
		int GetMin() const;
		int GetSize() const;

	private:
		vector<int> numbers_;
};

#endif