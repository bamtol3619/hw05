#include <vector>
#include <map>
#include <iostream>
#include <stdlib.h>
#include "message_book.h"

using namespace std;

int main(){
	MessageBook Book;
	while(true){
		string input;
		cin >> input;
		if(input=="quit")	break;
		else if(input=="add"){
			string phone_num,messages;
			cin >> phone_num;
			getline(cin,messages);
			Book.AddMessage(atoi(phone_num.c_str()), messages);
		}
		else if(input=="delete"){
			string phone_num;
			cin >> phone_num;
			Book.DeleteMessage(atoi(phone_num.c_str()));			
		}
		else if(input=="print"){
			string phone_num;
			cin >> phone_num;
			Book.PrintMessage(atoi(phone_num.c_str()));
		}
		else if(input=="list"){
			Book.ListMessage();
		}		
	}
	return 0;
}