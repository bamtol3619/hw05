#include <iostream>
#include <vector>
#include "draw_shape.h"

using namespace std;

int main(){
	size_t row,col;
	cin >> row;
	cin >> col;
	Canvas canvas(row, col);
	canvas.Draw(cout);
	while(true){
		string input;
		cin >> input;
		if(input=="quit")	break;
		else if(input=="add"){
			Shape shape;
			string Stype;
			int Sx,Sy,Swidth=0,Sheight;
			char Sbrush;
			cin >> Stype;
			if(Stype=="rect"){
				shape.type=RECTANGLE;
				cin >> Sx >> Sy >> Swidth >> Sheight >> Sbrush;
			}
			else if(Stype=="tri_up"){
				shape.type=TRIANGLE_UP;
				cin >> Sx >> Sy >> Sheight >> Sbrush;
			}
			else if(Stype=="tri_down"){
				shape.type=TRIANGLE_DOWN;
				cin >> Sx >> Sy >> Sheight >> Sbrush;
			}
			shape.x=Sx;
			shape.y=Sy;
			shape.width=Swidth;
			shape.height=Sheight;
			shape.brush=Sbrush;
			canvas.AddShape(shape);
		}
		else if(input=="delete"){
			int index;
			cin >> index;
			canvas.DeleteShape(index);
		}
		else if(input=="draw"){
			canvas.Draw(cout);
		}
		else if(input=="dump"){
			canvas.Dump(cout);
		}
	}
	return 0;
}