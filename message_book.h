#include <vector>
#include <map>
#include <iostream>

using namespace std;

#ifndef __hw05__message_book__
#define __hw05__message_book__

class MessageBook {
	public:
		MessageBook();
		~MessageBook();
		void AddMessage(int number, const string& message);
		void DeleteMessage(int number);
		void PrintMessage(int number);
		void ListMessage();
		
	private:
		map<int, string> messages_;
};

#endif