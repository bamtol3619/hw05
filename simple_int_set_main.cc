#include <iostream>
#include "simple_int_set.h"

using namespace std;

enum EOperateType
{
    Invalid = -1,
    
    Add,
    Sub,
    Mul,
};
typedef enum EOperateType OperateType;

int main(void)
{
    while(true)
    {
    set<int> set1;
	if(!InputSet(cin,&set1))	break;	//incorrect input
	OperateType type = Invalid;
	char operation;
	cin >> operation;
	if(operation=='+')	type = Add;
	else if(operation=='-')	type = Sub;
	else if(operation=='*')	type = Mul;
	else break;	//incorrect input
	set<int> set2;
	if(!InputSet(cin,&set2))	break;	//incorrect input
	set<int> result;
	if(type==Add)	result=SetUnion(set1, set2);
    else if(type==Sub)	result=SetDifference(set1, set2);
    else if(type==Mul)	result=SetIntersection(set1, set2);
	OutputSet(result, cout);
    }
    return 0;
}