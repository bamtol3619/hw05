// simple_int_set.h
#include <iostream>
#include <set>

using namespace std;

#ifndef __hw05__simple_int_set__
#define __hw05__simple_int_set__

set<int> SetUnion(const set<int>& set0, const set<int>& set1);
set<int> SetDifference(const set<int>& set0, const set<int>& set1);
set<int> SetIntersection(const set<int>& set0, const set<int>& set1);

bool InputSet(istream& in, set<int>* s);
// return false when input is incorrect
// (input must be like { 1 2 3 })
void OutputSet(const set<int>& s, ostream& out);

#endif