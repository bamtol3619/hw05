#include <iostream>
#include <vector>
#include <string>
#include "draw_shape.h"

using namespace std;


Canvas::Canvas(size_t row, size_t col){
	row_=row;
	col_=col;
}

Canvas::~Canvas(){}

int Canvas::AddShape(const Shape &s){  // Return the index of the shape.
	if(s.x < 0 || s.x > row_ || s.y < 0 || s.y > col_)	cout << "error out of canvas" << endl;	//기본적으로 캔버스 바깥의 점은 제외
	else if(s.type==RECTANGLE){
		if(s.width%2==0 || s.height%2==0)	cout << "error invalid input" << endl;	//RECTANGLE은 너비,높이가 홀수이어야 한다.
		else if(s.x-s.height/2 < 0 || s.x+s.height/2 > row_-1 || s.y-s.width/2 < 0 || s.y+s.width/2 > col_-1)	cout << "error out of canvas" << endl;	//너비,높이가 캔버스 밖을 나간 경우
		else	shapes_.push_back(s);	//그 외에는 ok
	}
	else if(s.type==TRIANGLE_UP){
		if(s.x+(s.height-1) > row_-1 || s.y-(s.height-1) < 0 || s.y+(s.height-1) > col_-1)	cout << "error out of canvas" << endl;	//너비,높이가 캔버스 밖을 나간 경우
		else shapes_.push_back(s);	//그 외에는 ok
	}
	else if(s.type==TRIANGLE_DOWN){
		if(s.x-(s.height-1) < 0 || s.y-(s.height-1) < 0 || s.y+(s.height-1) > col_-1)	cout << "error out of canvas" << endl;	//너비,높이가 캔버스 밖을 나간 경우
		else shapes_.push_back(s);	//그 외에는 ok
	}
	return 0;
}

void Canvas::DeleteShape(int index){
	if(index >=0 && index < shapes_.size())	shapes_.erase(shapes_.begin()+index);
}

void Canvas::Draw(ostream& os){
	chars_.clear();
	for(int i=0;i<row_;++i){	//chars_ 초기화
		string temp;
		for(int j=0;j<col_;++j){
			char c='.';
			temp.push_back(c);
		}
		chars_.push_back(temp);
	}

	for(vector<Shape>::iterator it = shapes_.begin();it!=shapes_.end();++it){
		if((*it).type==RECTANGLE){
			for(int i=0;i<row_;++i){
				if(i >= (*it).x-(*it).height/2 && i <= (*it).x+(*it).height/2){
					for(int j=0;j<col_;++j){
						if(j >= (*it).y-(*it).width/2 && j <= (*it).y+(*it).width/2)	chars_[i][j]=(*it).brush;
					}
				}
			}
		}
		else if((*it).type==TRIANGLE_UP){
			for(int i=0;i<row_;++i){
				if(i >= (*it).x && i <= (*it).x+((*it).height-1)){
					for(int j=0;j<col_;++j){
						if(j >= (*it).y-(i-(*it).x) && j <= (*it).y+(i-(*it).x))	chars_[i][j]=(*it).brush;
					}
				}
			}
		}
		else if((*it).type==TRIANGLE_DOWN){
			for(int i=0;i<row_;++i){
				if(i >= (*it).x-((*it).height-1) && i <= (*it).x){
					for(int j=0;j<col_;++j){
						if(j >= (*it).y-((*it).x-i) && j <= (*it).y+((*it).x-i))	chars_[i][j]=(*it).brush;
					}
				}
			}
		}
	}

	os << " ";
	for(int i=0;i<col_;++i)	os << (i%10);
	os << endl;
	for(int i=0;i<row_;++i)	os << (i%10) << chars_[i] << endl;
}

void Canvas::Dump(ostream& os){
	int i=0;
	for(vector<Shape>::iterator it = shapes_.begin();it!=shapes_.end();++it){
		if((*it).type==RECTANGLE)	os << i << " rect " << (*it).x << " " << (*it).y << " " << (*it).width << " " << (*it).height << " " << (*it).brush << endl;
		else if((*it).type==TRIANGLE_UP)	os << i << " tri_up " << (*it).x << " " << (*it).y << " " << (*it).height << " " << (*it).brush << endl;
		else if((*it).type==TRIANGLE_DOWN)	os << i << " tri_down " << (*it).x << " " << (*it).y << " "  << (*it).height << " " << (*it).brush << endl;
		++i;
	}
}