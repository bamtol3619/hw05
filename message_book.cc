#include <vector>
#include <map>
#include <iostream>
#include "message_book.h"
#include <string>

using namespace std;

MessageBook::MessageBook(){map<int, string> messages_;}
MessageBook::~MessageBook(){}

void MessageBook::AddMessage(int number, const string& message){
	if((messages_.insert(make_pair(number,message)).second==false))	messages_[number]=message;	
}

void MessageBook::DeleteMessage(int number){
	if(messages_.find(number)!=messages_.end())	messages_.erase(messages_.find(number));
}

void MessageBook::PrintMessage(int number){
	if(messages_.find(number)!=messages_.end())	cout << messages_[number].substr(1,messages_[number].length()-1);
	cout << endl;
}

void MessageBook::ListMessage(){
	for(map<int, string>::iterator it=messages_.begin();it!=messages_.end();++it)	cout << it->first << ":" << it->second << endl;

}